package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.DevelopmentData;
import com.epam.rd.java.basic.task8.Game;
import org.xml.sax.helpers.DefaultHandler;


import javax.xml.stream.*;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	HashSet<Game> set = new HashSet<>();
	private String xmlFileName;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}
	public ArrayList<Game> getGames() throws XMLStreamException, FileNotFoundException {
		XMLInputFactory factory = XMLInputFactory.newInstance();
		XMLEventReader reader = factory.createXMLEventReader((new FileInputStream(xmlFileName)));
		while (reader.hasNext()){
			XMLEvent event = reader.nextEvent();
			if(event.isStartElement()){
				if(event.asStartElement().getName().getLocalPart().equals("a")||
						event.asStartElement().getName().getLocalPart().equals("b")||
						event.asStartElement().getName().getLocalPart().equals("c")) {
					Game game = new Game();
					reader.nextEvent();
					reader.nextEvent();
					event=reader.nextEvent();
					game.setName(event.asCharacters().getData());
					while (!event.isStartElement()){
						event=reader.nextEvent();
					}
					event= reader.nextEvent();
					game.setCamera(Game.Camera.valueOf(event.asCharacters().getData()));
					while (!event.isStartElement()){
						event=reader.nextEvent();
					}event= reader.nextEvent();
					game.setGenre(event.asCharacters().getData());
					while (!event.isStartElement()){
						event=reader.nextEvent();
					}event= reader.nextEvent();
					while (!event.isStartElement()){
						event=reader.nextEvent();
					}event= reader.nextEvent();
					DevelopmentData data = new DevelopmentData();
					data.setBudget(Long.parseLong(event.asCharacters().getData()));
					while (!event.isStartElement()){
						event=reader.nextEvent();
					}event= reader.nextEvent();
					data.setReleaseYear(Integer.parseInt(event.asCharacters().getData()));
					while (!event.isStartElement()){
						event=reader.nextEvent();
					}event= reader.nextEvent();
					data.setFirstAnnounce(Integer.parseInt(event.asCharacters().getData()));
					game.setDevelopmentData(data);
					while (!event.isStartElement()){
						event=reader.nextEvent();
					}event= reader.nextEvent();
					game.setPlatform(Game.Platform.valueOf(event.asCharacters().getData()));
					while (!event.isStartElement()){
						event=reader.nextEvent();
					}event= reader.nextEvent();
					game.setDeveloper(event.asCharacters().getData());
					while (!event.isStartElement()){
						event=reader.nextEvent();
					}event= reader.nextEvent();
					game.setStore(Game.Store.valueOf(event.asCharacters().getData()));
					set.add(game);
				}
			}
		}
		ArrayList<Game> list = new ArrayList<>();
		for(Game g :set)
			list.add(g);
		return list;
	}

	public void createXML(String path, ArrayList<Game> games) throws FileNotFoundException, XMLStreamException {
		XMLOutputFactory factory = XMLOutputFactory.newInstance();
		XMLStreamWriter writer = factory.createXMLStreamWriter(new FileOutputStream(path));
		writer.writeStartDocument();
		writer.writeStartElement("games");
		writer.writeAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
		writer.writeAttribute("xsi:noNamespaceSchemaLocation","input.xsd");
		writer.writeAttribute("xmlns","http://www.nure.ua");
		writer.writeCharacters("\n");
		for(int i =0;i<games.size();i++){
			writer.writeStartElement(Character.toString(97+i));
			writer.writeCharacters("\n");
			writer.writeStartElement("name");
			writer.writeCharacters(games.get(i).getName());
			writer.writeEndElement();
			writer.writeCharacters("\n");
			writer.writeStartElement("camera");
			writer.writeCharacters(games.get(i).getCamera().name());
			writer.writeEndElement();
			writer.writeCharacters("\n");
			writer.writeStartElement("genre");
			writer.writeCharacters(games.get(i).getGenre());
			writer.writeEndElement();
			writer.writeCharacters("\n");
			writer.writeStartElement("DevelopmentData");
			writer.writeCharacters("\n");
			writer.writeStartElement("budget");
			writer.writeCharacters(""+games.get(i).getDevelopmentData().getBudget());
			writer.writeEndElement();
			writer.writeCharacters("\n");
			writer.writeStartElement("releaseYear");
			writer.writeCharacters(""+games.get(i).getDevelopmentData().getReleaseYear());
			writer.writeEndElement();
			writer.writeCharacters("\n");
			writer.writeStartElement("firstAnnounce");
			writer.writeCharacters(""+games.get(i).getDevelopmentData().getFirstAnnounce());
			writer.writeEndElement();
			writer.writeCharacters("\n");
			writer.writeEndElement();
			writer.writeCharacters("\n");
			writer.writeStartElement("platform");
			writer.writeCharacters(games.get(i).getPlatform().name());
			writer.writeEndElement();
			writer.writeCharacters("\n");
			writer.writeStartElement("developer");
			writer.writeCharacters(games.get(i).getDeveloper());
			writer.writeEndElement();
			writer.writeCharacters("\n");
			writer.writeStartElement("Store");
			writer.writeCharacters(games.get(i).getStore().name());
			writer.writeEndElement();
			writer.writeCharacters("\n");
			writer.writeEndElement();
			writer.writeCharacters("\n");
		}
		writer.writeEndElement();
		writer.writeEndDocument();
	}

}