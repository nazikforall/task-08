package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.DevelopmentData;
import com.epam.rd.java.basic.task8.Game;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;
	private HashSet<Game> set = new HashSet<>();

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		HashSet<Game> games = new HashSet<>();
		String str="";
		for(char cha : ch){
			str+=cha;
		}
		for(int i =0;i<3;i++){
			String gameStr =str.substring(str.indexOf(String.format("<%s>",Character.toString(97+i))),
					str.indexOf(String.format("</%s>",Character.toString(97+i))));
			Game game = new Game();
			game.setName(gameStr.substring(gameStr.indexOf("e>")+2,gameStr.indexOf("</name>")));
			gameStr=gameStr.substring(gameStr.indexOf("<camera>"));
			game.setCamera(Game.Camera.valueOf(gameStr.substring(gameStr.indexOf("a>")+2,
					gameStr.indexOf("</camera>"))));
			gameStr=gameStr.substring(gameStr.indexOf("<genre>"));
			game.setGenre(gameStr.substring(gameStr.indexOf("e>")+2,gameStr.indexOf("</genre>")));
			gameStr=gameStr.substring(gameStr.indexOf("<budget>"));
			DevelopmentData data = new DevelopmentData();
			data.setBudget(Long.parseLong(gameStr.substring(gameStr.indexOf("t>")+2,
					gameStr.indexOf("</budget>"))));
			gameStr=gameStr.substring(gameStr.indexOf("<releaseYear>"));
			data.setReleaseYear(Integer.parseInt(gameStr.substring(gameStr.indexOf("r>")+2,
					gameStr.indexOf("</releaseYear>"))));
			gameStr=gameStr.substring(gameStr.indexOf("<firstAnnounce>"));
			data.setFirstAnnounce(Integer.parseInt(gameStr.substring(gameStr.indexOf("e>")+2,
					gameStr.indexOf("</firstAnnounce>"))));
			gameStr=gameStr.substring(gameStr.indexOf("<platform>"));
			game.setDevelopmentData(data);
			game.setPlatform(Game.Platform.valueOf(gameStr.substring(gameStr.indexOf("m>")+2,
					gameStr.indexOf("</platform>"))));
			gameStr=gameStr.substring(gameStr.indexOf("<developer>"));
			game.setDeveloper(gameStr.substring(gameStr.indexOf("r>")+2,
					gameStr.indexOf("</developer>")));
			gameStr=gameStr.substring(gameStr.indexOf("<Store>"));
			game.setStore(Game.Store.valueOf(gameStr.substring(gameStr.indexOf("e>")+2,
					gameStr.indexOf("</Store>"))));
			set.add(game);

		}
	}

	public ArrayList<Game> getList() {
		ArrayList<Game> list = new ArrayList<>();
		for(Game g :set)
			list.add(g);
		return list;
	}

	public void createXML(String path, ArrayList<Game> games) throws FileNotFoundException, XMLStreamException {
		XMLOutputFactory factory = XMLOutputFactory.newInstance();
		XMLStreamWriter writer = factory.createXMLStreamWriter(new FileOutputStream(path));
		writer.writeStartDocument();
		writer.writeStartElement("games");
		writer.writeAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
		writer.writeAttribute("xsi:noNamespaceSchemaLocation","input.xsd");
		writer.writeAttribute("xmlns","http://www.nure.ua");
		writer.writeCharacters("\n");
		for(int i =0;i<games.size();i++){
		writer.writeStartElement(Character.toString(97+i));
			writer.writeCharacters("\n");
			writer.writeStartElement("name");
			writer.writeCharacters(games.get(i).getName());
			writer.writeEndElement();
			writer.writeCharacters("\n");
		 	writer.writeStartElement("camera");
			writer.writeCharacters(games.get(i).getCamera().name());
			writer.writeEndElement();
			writer.writeCharacters("\n");
			writer.writeStartElement("genre");
			writer.writeCharacters(games.get(i).getGenre());
			writer.writeEndElement();
			writer.writeCharacters("\n");
			writer.writeStartElement("DevelopmentData");
				writer.writeCharacters("\n");
				writer.writeStartElement("budget");
				writer.writeCharacters(""+games.get(i).getDevelopmentData().getBudget());
				writer.writeEndElement();
				writer.writeCharacters("\n");
				writer.writeStartElement("releaseYear");
				writer.writeCharacters(""+games.get(i).getDevelopmentData().getReleaseYear());
				writer.writeEndElement();
				writer.writeCharacters("\n");
				writer.writeStartElement("firstAnnounce");
				writer.writeCharacters(""+games.get(i).getDevelopmentData().getFirstAnnounce());
				writer.writeEndElement();
				writer.writeCharacters("\n");
				writer.writeEndElement();
			writer.writeCharacters("\n");
			writer.writeStartElement("platform");
			writer.writeCharacters(games.get(i).getPlatform().name());
			writer.writeEndElement();
			writer.writeCharacters("\n");
			writer.writeStartElement("developer");
			writer.writeCharacters(games.get(i).getDeveloper());
			writer.writeEndElement();
			writer.writeCharacters("\n");
			writer.writeStartElement("Store");
			writer.writeCharacters(games.get(i).getStore().name());
			writer.writeEndElement();
			writer.writeCharacters("\n");
			writer.writeEndElement();
			writer.writeCharacters("\n");
		}
		writer.writeEndElement();
		writer.writeEndDocument();
	}

}