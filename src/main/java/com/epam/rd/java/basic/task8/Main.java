package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		ArrayList<Game> list = domController.getGames();

		Collections.sort(list, (a,b)->{
			if(a.getDevelopmentData().getBudget()>=b.getDevelopmentData().getBudget())return 1; else return -1;
		});
        // save
		String outputXmlFile = "output.dom.xml";
		domController.createXML(outputXmlFile,list);


		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////

		// get
		SAXController saxController = new SAXController(xmlFileName);
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser parser= factory.newSAXParser();
		parser.parse(new File(xmlFileName),saxController);
		ArrayList<Game> list2 = saxController.getList();
		// sort  (case 2)
		// PLACE YOUR CODE HERE
		Collections.sort(list2,(a,b)->{
			if(a.getDevelopmentData().getReleaseYear()<=b.getDevelopmentData().getReleaseYear())return -1;else return 1;
		});

		// save
		outputXmlFile = "output.sax.xml";
		saxController.createXML(outputXmlFile,list2);
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		ArrayList<Game> list3 = staxController.getGames();
		for(Game g : list3)
			System.out.println(g.getName());
		// sort  (case 3)
		Collections.sort(list3,(a,b)->{
			return a.getName().compareTo(b.getName());
		});

		System.out.println("-----------");
		for (Game g : list3)
			System.out.println(g.getName());
		// save
		outputXmlFile = "output.stax.xml";
		staxController.createXML(outputXmlFile,list3);
	}

}
