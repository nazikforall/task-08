package com.epam.rd.java.basic.task8;

public class Game{
   private String name;
   private Camera camera;
   private String genre;
   private DevelopmentData developmentData;
   private Platform platform;
   private String developer;
   private Store store;


    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public DevelopmentData getDevelopmentData() {
        return developmentData;
    }
    public void setDevelopmentData(DevelopmentData developmentData) {
        this.developmentData = developmentData;
    }
    public Camera getCamera(){
        return this.camera;
    }
    public void setCamera(Camera camera) {
        this.camera = camera;
    }
    public String getGenre() {
        return genre;
    }
    public void setGenre(String genre) {
        this.genre = genre;
    }
    public String getDeveloper() {
        return developer;
    }
    public void setDeveloper(String developer) {
        this.developer = developer;
    }
    public Platform getPlatform() {
        return platform;
    }
    public void setPlatform(Platform platform) {
        this.platform = platform;
    }
    public Store getStore() {
        return store;
    }
    public void setStore(Store store) {
        this.store = store;
    }

    @Override
    public int hashCode() {
        int res=0;
        res+=getName().hashCode()*17;
        res+=getCamera().ordinal()*17;
        res+=getGenre().hashCode()*17;
        res+=getDeveloper().hashCode()*17;
        res+=getPlatform().ordinal()*17;
        res+=getStore().ordinal()*17;
        res+=getDevelopmentData().hashCode()*17;
        return res;
    }

    @Override
    public boolean equals(Object obj) {
        if(!obj.getClass().getName().equals(this.getClass().getName()))return false;
        return obj.hashCode()==this.hashCode();
    }

    public enum Camera {
        First_person,
        Third_person,
        Top_camera
    }
   public enum Platform{
        PC,PS5,Xbox_Series_X,Switch,Multiplatform
    }
   public enum Store{
        PSStore,Microsoft_Store,
        Steam, Epic_Games
    }
}
