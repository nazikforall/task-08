package com.epam.rd.java.basic.task8;

public class DevelopmentData{
       private long budget;
       private int releaseYear;
       private int firstAnnounce;

    public long getBudget() {
        return budget;
    }

    public void setBudget(long budget) {
        this.budget = budget;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public int getFirstAnnounce() {
        return firstAnnounce;
    }

    public void setFirstAnnounce(int firstAnnounce) {
        this.firstAnnounce = firstAnnounce;
    }

    @Override
    public int hashCode() {
        int res=0;
        res+=getFirstAnnounce()*17;
        res+=getReleaseYear()*17;
        res+=getBudget()*17;
        return res;
    }
}
