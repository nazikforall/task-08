package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.DevelopmentData;
import com.epam.rd.java.basic.task8.Game;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection.*;

/**
 * Controller for DOM parser.
 */
public class 	DOMController {

	private String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public ArrayList<Game> getGames() throws ParserConfigurationException, IOException, SAXException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder builder =factory.newDocumentBuilder();
		ArrayList<Game> res = new ArrayList<>();
		Document document = builder.parse(new File(xmlFileName));
		ArrayList<NodeList> nodeLists = new ArrayList<>();
		nodeLists.add( document.getElementsByTagName("a"));
		nodeLists.add( document.getElementsByTagName("b"));
		nodeLists.add( document.getElementsByTagName("c"));
		for(int i =0;i<nodeLists.size();i++){
		Game game = new Game();
		game.setName( nodeLists.get(i).item(0).getChildNodes().item(1).getTextContent().trim());
		game.setCamera(Game.Camera.valueOf(nodeLists.get(i).item(0).getChildNodes().item(3).getTextContent().trim()));
		game.setGenre( nodeLists.get(i).item(0).getChildNodes().item(5).getTextContent().trim());
		DevelopmentData data = new DevelopmentData();
		data.setBudget(Integer.parseInt(nodeLists.get(i).item(0).getChildNodes().
				item(7).getChildNodes().item(1).getTextContent().trim()));
		data.setReleaseYear(Integer.parseInt(nodeLists.get(i).item(0).getChildNodes().
				item(7).getChildNodes().item(3).getTextContent().trim()));
		data.setFirstAnnounce(Integer.parseInt(nodeLists.get(i).item(0).getChildNodes().
				item(7).getChildNodes().item(5).getTextContent().trim()));
		game.setPlatform(Game.Platform.valueOf(nodeLists.get(i).item(0).getChildNodes().item(9).getTextContent().trim()));
		game.setDeveloper(nodeLists.get(i).item(0).getChildNodes().item(11).getTextContent().trim());
		game.setStore(Game.Store.valueOf(nodeLists.get(i).item(0).getChildNodes().item(13).getTextContent().trim()));
		game.setDevelopmentData(data);
		res.add(game);
		}
		return res;
	}

	public void createXML(String path, ArrayList<Game> list) throws ParserConfigurationException, TransformerException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder =factory.newDocumentBuilder();
		Document document = builder.newDocument();
		Element rootElement = document.createElementNS("http://www.nure.ua","games");
		document.appendChild(rootElement);
		rootElement.setAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
		rootElement.setAttribute("xsi:noNamespaceSchemaLocation","input.xsd");
		for(int i =0;i<list.size();i++){
			rootElement.appendChild(getNodeGame(document,list.get(i),i));
		}
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT,"yes");
		DOMSource source =new DOMSource(document);
		StreamResult file = new StreamResult(new File(path));
		transformer.transform(source,file);

	}

	private Node getNodeGame(Document document,Game game, int iteration) throws ParserConfigurationException {
		Element elGame = document.createElement(Character.toString(97 + iteration));
		elGame.appendChild(getGameElement(document,elGame,"name",game.getName()));
		elGame.appendChild(getGameElement(document,elGame,"camera",game.getCamera().name()));
		elGame.appendChild(getGameElement(document,elGame,"genre",game.getGenre()));
		elGame.appendChild(getDevData(document,elGame,"DevelopmentData",game.getDevelopmentData()));
		elGame.appendChild(getGameElement(document,elGame,"platform",game.getPlatform().name()));
		elGame.appendChild(getGameElement(document,elGame,"developer",game.getDeveloper()));
		elGame.appendChild(getGameElement(document,elGame,"Store",game.getStore().name()));
		return elGame;
	}
	private Node getGameElement(Document doc,Element el,String name,String value){
		Element node = doc.createElement(name);
		node.appendChild(doc.createTextNode(value));
		return node;
	}
	private Node getDevData(Document doc,Element el,String name, DevelopmentData data){
		Element devData = doc.createElement(name);
		devData.appendChild(getGameElement(doc,devData,"budget",""+data.getBudget()+""));
		devData.appendChild(getGameElement(doc,devData,"releaseYear",""+data.getReleaseYear()+""));
		devData.appendChild(getGameElement(doc,devData,"firstAnnounce",""+data.getFirstAnnounce()+""));
		return devData;
	}
}
